// Deafault App component that all other compents are rendered through
function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
class Game extends React.Component{
render(){
return(  
        <React.Fragment>
        <h1>Welcome to the sports game starter</h1>
        <h2> We are here live at the {this.props.venue}</h2>
        <div className= 'team'>
        <Team name= 'Blue Team' logo='./images/blue-team2.jpg'/>
        <Team name= 'Red Team'logo='./images/red-team-3.jpg'/>
        </div>
        </React.Fragment>
    )
}
}
class Team extends React.Component{
    state ={
      shots: 0,
      shotsMade: 0,
      points: 0,
      scored:randomInt(1,3),
      shotPercentage:0
    }
    audioFiles = {
  kobe: new Audio("./audio-files/Swish.mp3"),
  shaq: new Audio("./audio-files/Back+Board.mp3")
    }
  shooting = () =>{
    if(this.state.scored === 2){
      this.setState((state)=>({
        shots: state.shots +1,
        shotsMade: state.shotsMade +1,
        points: state.points +2,
        scored: randomInt(1,3),
      }))
      this.audioFiles.kobe.play()
    }
    else{
        this.setState((state)=>({
            shots: state.shots +1,
            scored: randomInt(1,3),
        }))
        console.log(randomInt(1,3))
        this.audioFiles.shaq.play()
    }
  }
  getPercentage() {
    const { shotsMade, shots } = this.state;
    const pct = Math.round(((shotsMade/shots ) * 100))
    return Number.isNaN(pct) ? "" : `${pct}%`;
  }
  render() {
      return (
        <div className='stats'>
      <h3>{this.props.name}</h3>
     <img src={this.props.logo} />
      <br></br>
      <span>Shots = {this.state.shots} </span>
      <br></br>
      <span>Shots Made = {this.state.shotsMade} </span>
      <br></br>
      <span>Points = {this.state.points} </span>
      <br></br>
      <span> Shot Percentage = {this.getPercentage()} </span>
      <br></br>
    <button onClick={() => {
      this.shooting()
    }}>Shoot</button>
    </div>
      )
    }   
        };
  function App(){
    return (
      <div>
        <Game venue ="Thunder Dome" />
      </div>
    )
  }  //Render the application
  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );